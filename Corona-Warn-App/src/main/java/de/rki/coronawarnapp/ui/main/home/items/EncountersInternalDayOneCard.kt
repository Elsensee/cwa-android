package de.rki.coronawarnapp.ui.main.home.items

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import de.rki.coronawarnapp.R
import de.rki.coronawarnapp.databinding.HomeEncountersInternalDayOneCardLayoutBinding
import de.rki.coronawarnapp.ui.main.home.HomeAdapter
import de.rki.coronawarnapp.ui.main.home.items.EncountersInternalDayOneCard.Item
import de.rki.coronawarnapp.util.lists.diffutil.HasPayloadDiffer

class EncountersInternalDayOneCard(parent: ViewGroup) : HomeAdapter.HomeItemVH<Item, HomeEncountersInternalDayOneCardLayoutBinding>(
    R.layout.home_card_container_layout, parent
) {

    override val viewBinding = lazy {
        HomeEncountersInternalDayOneCardLayoutBinding.inflate(layoutInflater, itemView.findViewById(R.id.card_container), true)
    }

    @SuppressLint("ClickableViewAccessibility") // chart is not focusable
    override val onBindData: HomeEncountersInternalDayOneCardLayoutBinding.(
        item: Item,
        payloads: List<Any>
    ) -> Unit = { item, payloads ->
        
        val onClickListener = View.OnClickListener {
            val curItem = payloads.filterIsInstance<Item>().singleOrNull() ?: item
            curItem.onClickAction(item)
        }

        itemView.setOnClickListener(onClickListener)

        textMid.text = context.getString(R.string.main_card_encounters_day_one_mid, item.rpiAmount)

        if (item.dataTooOldForFiveDayChart) {
            textTop.text = context.getString(R.string.main_card_encounters_day_one_chart_empty_top)
            textBottom.text = context.getString(R.string.main_card_encounters_day_one_chart_empty_bottom)
            textMicrotutorial.text = context.getString(R.string.main_card_encounters_day_one_chart_empty_microtutorial)
        }

    }

    data class Item(
        val rpiAmount: Long,
        val dataTooOldForFiveDayChart: Boolean,
        val onClickAction: (Item) -> Unit
    ) : HomeItem, HasPayloadDiffer {
        override val stableId: Long = Item::class.java.name.hashCode().toLong()

        override fun diffPayload(old: Any, new: Any): Any? = if (old::class == new::class) new else null
    }
}
