package de.rki.coronawarnapp.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import de.rki.coronawarnapp.R

class RpiHostActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rpi_host)
        supportActionBar?.title = getString(R.string.main_card_encounters)
        supportActionBar?.subtitle = getString(R.string.rpi_activity_subtitle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = .0f
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0, 0)
    }
}
