package de.rki.coronawarnapp.ui.main.home.items

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.view.ViewGroup
import de.rki.coronawarnapp.R
import de.rki.coronawarnapp.databinding.HomeEncountersExternalCardLayoutBinding
import de.rki.coronawarnapp.ui.main.home.HomeAdapter
import de.rki.coronawarnapp.ui.main.home.items.EncountersExternalCard.Item
import de.rki.coronawarnapp.util.ExposureNotificationProvider.Companion.MICROG_PACKAGE_NAME
import de.rki.coronawarnapp.util.lists.diffutil.HasPayloadDiffer
import de.rki.coronawarnapp.util.ui.createMicrogImageSpan
import de.rki.coronawarnapp.util.ui.replaceWithFormatted
import timber.log.Timber

class EncountersExternalCard(parent: ViewGroup) : HomeAdapter.HomeItemVH<Item, HomeEncountersExternalCardLayoutBinding>(
    R.layout.home_card_container_layout, parent
) {

    override val viewBinding = lazy {
        HomeEncountersExternalCardLayoutBinding.inflate(layoutInflater, itemView.findViewById(R.id.card_container), true)
    }

    override val onBindData: HomeEncountersExternalCardLayoutBinding.(
        item: Item,
        payloads: List<Any>
    ) -> Unit = { item, payloads ->
        
        val onClickListener = View.OnClickListener {
            val curItem = payloads.filterIsInstance<Item>().singleOrNull() ?: item
            curItem.onClickAction(item)
        }

        itemView.setOnClickListener(onClickListener)
        openEncountersButton.setOnClickListener(onClickListener)

        try {
            val packageManager = context.packageManager

            val intent = packageManager.getLaunchIntentForPackage(MICROG_PACKAGE_NAME)
            if (intent == null) {
                Timber.w("microG package cannot be launched")
            } else {
                val activityList: List<ResolveInfo> = packageManager.queryIntentActivities(intent, 0)
                val activityName = activityList[0].loadLabel(packageManager)

                val builder = SpannableStringBuilder(context.getString(R.string.main_encounters_external, activityName))

                mainCardContentBody.text =
                    replaceWithFormatted(
                        builder,
                        '▶', "▶",
                        createMicrogImageSpan(context = super.context as Activity)
                    )
            }


        } catch (e: PackageManager.NameNotFoundException) {
            Timber.w(e, "microG package is missing")
        }
    }

    data class Item(val onClickAction: (Item) -> Unit) : HomeItem, HasPayloadDiffer {
        override val stableId: Long = Item::class.java.name.hashCode().toLong()

        override fun diffPayload(old: Any, new: Any): Any? = if (old::class == new::class) new else null
    }
}
