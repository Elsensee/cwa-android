#!/bin/bash

set -e

cd fdroidrepo
# create folders to hopefully not f*ck up permissions by handing them to docker
mkdir -p unsigned repo build
# grab current repo status from server
rsync -av --delete  --no-owner --no-group bubu1.eu:/srv/http/cctg/fdroid/repo/ repo
rsync -av --delete  --no-owner --no-group bubu1.eu:/srv/http/cctg/fdroid/archive/ archive
# build container and run build in it
# we need the `unsigned` dir as an output folder inside the container,
# `repo` is handed to the container as a read-only dir to not rebuild an already existing release
# `build` is passed into the container to populate it for running `fdroid update` later.
docker build -t cctg .
docker run -it \
    --volume ${PWD}/unsigned:/home/builder/unsigned \
    --volume ${PWD}/repo:/home/builder/repo:ro \
    --volume ${PWD}/build:/home/builder/build \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    cctg
# sign the release outside the container and build the repo structure
fdroid publish
fdroid update
# update pages repo
pushd ../../cctg-pages/
git checkout main
git fetch
git reset --hard origin/main
popd
rm -r ../../cctg-pages/fdroid/repo
cp -r ./repo ../../cctg-pages/fdroid/
pushd ../../cctg-pages/
git add .
git commit -am "update version"
git push
popd
# push the new release to the server
fdroid deploy

# fix up server side permissions
cd ..
rsync fdroidrepo/repo/ bubu1.eu:/srv/http/cctg/fdroid/repo --no-owner --no-group -r
