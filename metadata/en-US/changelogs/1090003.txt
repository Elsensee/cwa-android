* fix crash when opening microG ENF settings
* fix crash during exposure check
* some translation fixes (added polish translation of our additions)
