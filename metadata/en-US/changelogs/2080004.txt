– Certificate usability improvements (show standardized names, open from test result) (CWA)
– Display certificate expiry date and verify electronic signature (CWA 2.7.x)
